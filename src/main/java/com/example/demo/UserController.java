package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@GetMapping
	private void get() {
		System.out.println("Hello");
	}
	@PostMapping
	private void post() {
		System.out.println("Hi");
	}

}
